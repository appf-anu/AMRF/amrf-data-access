# AMRF data access code

This repository contains a few scripts to demonstrate programmatically downloading AMRF data. Refer to the table below for which script to use:

| I want to download (vvv) data using (>>>) language | R                   | Python               |
|----------------------------------------------------|---------------------|----------------------|
| Camera (Timeseries images)                         | `influx_demo.R`     | TBD                  |
| Weather station (InfluxDB/text)                    | `download_images.R` | `download_images.py` |
