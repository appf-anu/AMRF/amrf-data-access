#!/usr/bin/env python3
import urllib.request
import urllib.error
from datetime import date, timedelta
from tqdm import tqdm
import os

# example url:
# "https://img-anu.appf.org.au/file/traitcapture-cdn/cameras/appf_arraycamera/Capsule-4B-ArrayCamera01/Capsule-4B-ArrayCamera01~orig/2021/2021_12/2021_12_13/2021_12_13_16/Capsule-4B-ArrayCamera01~orig_2021_12_13_16_00_00_00.jpg"


def download_daily_images(image_base_url, camera_name, start_date, time_of_day, days_attempted):
    days_failed = 0

    # probably don't need these, but makes format more consistent
    mins = 0
    secs = 0

    # constants
    local_dir = "raw/"
    local_filename = "{camname}~orig_{year}_{month:02}_{day:02}_{hour:02}_00_00_00.jpg"
    ts_format = "{camname}/{camname}~orig/{year}/{year}_{month:02}/{year}_{month:02}_{day:02}/{year}_{month:02}_{day:02}_{hour:02}/{camname}~orig_{year}_{month:02}_{day:02}_{hour:02}_{minutes:02}_{seconds:02}_00.jpg"
    image_url = image_base_url + ts_format
    local_path = local_dir + local_filename

    os.makedirs(local_dir, exist_ok=True)

    for d in tqdm(range(days_attempted)):
        try:
            target = date.fromisoformat(start_date) + timedelta(d)
            remote = image_url.format(camname=camera_name, hour=time_of_day, year=target.year, month=target.month, day=target.day, minutes=mins, seconds=secs)
            url = urllib.request.Request(remote, headers={"User-Agent": "Mozilla/5.0"})
            with urllib.request.urlopen(url) as response, open(local_path.format(camname=camera_name, hour=time_of_day, year=target.year, month=target.month, day=target.day, minutes=mins, seconds=secs), 'wb') as local:
                local.write(response.read())
        except urllib.error.HTTPError:
            days_failed += 1

    print(days_attempted, "days attempted,", days_attempted - days_failed, "days images available")


if __name__ == '__main__':
    # download config
    image_base_url = "https://img.amrf.org.au/cameras/amrf/"  # base path to timestream-formatted directory
    time_of_day = 14  # time of day for each day's download, in 24-hour time as an int
    camera_name = "AMRF_PER_C01"  # name of camera in path
    start_date = "2022-05-24"  # first day to download
    days_attempted = 3  # how many days to download (including the first day)

    download_daily_images(image_base_url, camera_name, start_date, time_of_day, days_attempted)
